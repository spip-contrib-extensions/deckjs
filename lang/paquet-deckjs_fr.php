<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'deckjs_description' => 'Deckjs permet de réaliser des diaporamas facilement',
	'deckjs_nom' => 'deckjs',
	'deckjs_slogan' => '',
);
