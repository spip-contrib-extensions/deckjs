<?php
/**
 * Définit les autorisations du plugin deckjs
 *
 * @plugin     deckjs
 * @copyright  2016
 * @author     Tofulm
 * @licence    GNU/GPL
 * @package    SPIP\Deckjs\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function deckjs_autoriser(){}